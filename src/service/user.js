const repository = require('../repository');
const resultUtil = require('../util/result');
var mongoose = require('mongoose');
module.exports.getAll = function() {
  return new Promise(function(resolve, reject) {
    repository.User.find({}, function(err, users) {
      if (err) {
        let result = resultUtil.createErrorResult(
          '0x00',
          'Has error when lookup'
        );
        reject(result);
        return;
      }
      let result = resultUtil.createOkResult(users);
      resolve(result);
    });
  });
};
module.exports.createUser = function(user) {
  return new Promise(function(resolve, reject) {
    repository.User
      .insertOne(user)
      .then(function(user) {
        let result = resultUtil.createOkResult(user);
        resolve(result);
      })
      .catch(function(err) {
        let result = resultUtil.createErrorResult('0x01', 'This is error XXX');
        reject(result);
      });
  });
};
