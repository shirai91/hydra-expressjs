module.exports.createErrorResult = function(sysCode, sysMessage, sysDescription) {
  return {
    sysCode: sysCode,
    sysMessage: sysMessage,
    sysDescription
  };
};
module.exports.createOkResult = function(
  data,
  sysCode,
  sysMessage,
  sysDescription
) {
  return {
    data: data,
    sysCode: sysCode,
    sysMessage,
    sysDescription
  };
};
