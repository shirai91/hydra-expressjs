const OK = 200;
const CREATED = 201;
const RESOURCE_NOT_FOUND = 404;
const INTERNAL_SERVER_ERROR = 500;
/**
 * Make express response to 
 * @param {object} res express res object
 * @param {number} code request status code
 * @param {*} data response of request 
 * @param {object?} systemMetadata systemmetada, contain sysCode, sysMessage,sysDescription
 */
function createResponse(res, code, data, systemMetadata) {
  var responseTemplate = {
    systemCode: systemMetadata != null ? systemMetadata.sysCode : undefined,
    systemMessage:
      systemMetadata != null ? systemMetadata.sysMessage : undefined,
    systemDescription:
      systemMetadata != null ? systemMetadata.sysDescription : undefined,
    result: data
  };
  return res.status(code).json(responseTemplate);
}
module.exports.sendOk = function(res, data, systemMetadata) {
  createResponse(res, OK, data, systemMetadata);
};
/**
 * Send resource not found response
 * @param {object} res express res object
 * @param {object} systemMetadata system metadata
 */
module.exports.sendResourceNotFound = function(res, systemMetadata) {
  createResponse(res, RESOURCE_NOT_FOUND, systemMetadata);
};
/**
 * Send internal error response
 * @param {object} res express res object
 * @param {object} systemMetadata system metadata
 */
module.exports.sendInternalServerError = function(res, systemMetadata) {
  createResponse(res, INTERNAL_SERVER_ERROR, systemMetadata);
};
