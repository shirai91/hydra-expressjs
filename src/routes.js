const express = require('express');
const userApi = require('./api/user');
module.exports.initialize = function(app) {
  app.use('/user', userApi);
};
