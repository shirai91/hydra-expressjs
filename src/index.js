const express = require('express');
const app = express();
const middlewares = require('./middlewares');
const routes = require('./routes');
const hydra = require('./hydra');
middlewares.initialize(app);
routes.initialize(app);
hydra.initialize().then(function(hydraInstance) {
  hydraInstance.ready().then(() => {
    app.listen(3001, function() {
      console.log('server start at 3000');
    });
  });
});
