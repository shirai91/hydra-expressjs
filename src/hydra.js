const hydra = require('hydra');

module.exports.initialize = function() {
  return new Promise(function(resolve, reject) {
    hydra
      .init(`${__dirname}/config/hydra.json`)
      .then(function() {
        console.log('Hydra finished loading');
        hydra.registerService();
        hydra.getServices().then(function(service){
            console.log(service);
        });
        hydra.on('log', function(entry) {
          console.log('some log');
        });
        hydra.on('message', function(message) {
          console.log('some message');
        });
        resolve(hydra);
      })
      .catch(function(err) {
        reject(err);
      });
  });
};
