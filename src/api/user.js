const userService = require('../service/user');
const response = require('../util/response');
const express = require('express');
function getRoutes() {
  var routes = new express.Router();
  routes.get('/', getUsers);
  routes.post('/', createUser);
  return routes;
}
module.exports = getRoutes();
function getUsers(req, res) {
  userService
    .getAll()
    .then(function(result) {
      response.sendOk(res, result.data);
    })
    .catch(function(result) {
      response.sendInternalServerError(res, result);
    });
}
function createUser(req, res) {
  userService
    .createUser(req.body)
    .then(function(result) {
      response.sendOk(res,result.data);
    })
    .catch(function(result) {
      response.sendInternalServerError(res,result);
    });
}
