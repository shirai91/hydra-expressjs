const mongoose = require('mongoose');
const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;
const UserSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: false
  },
  deleted: {
    type: Boolean,
    required: false,
    default: false
  },
  password: {
    type: String,
    required: false
  }
});
UserSchema.statics.insertOne = function(user) {
  return new Promise(function(resolve, reject) {
    let User = mongoose.model('User', UserSchema);
    let newUser = new User(user);
    newUser
      .save()
      .then(function(result) {
        resolve(result);
      })
      .catch(function(err) {
        reject(err);
      });
  });
};
UserSchema.statics.updateOne = function(id, user) {
  return new Promise();
};
module.exports = UserSchema;
