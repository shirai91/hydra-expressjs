const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/demo');
const appRepository = {};
const repositoryPath = path.resolve(__dirname);
fs.readdirSync(repositoryPath).forEach(file => {
  console.log(file);
  if (file != 'index.js') {
    let name = file.replace('.js', '');
    let schema = require(path.resolve(__dirname,
      file));
    let model = mongoose.model(name,schema);
    appRepository[name] = model;
  }
});
module.exports = appRepository;
